package springmvc.java.dao;

import org.springframework.data.repository.CrudRepository;
import springmvc.java.domain.ProductType;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface ProductTypeDAO extends CrudRepository<ProductType,Long> {

    List<ProductType> findAll();
    ProductType findByTypeName(String typeName);

}
