package springmvc.java.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import springmvc.java.domain.Product;
import springmvc.java.domain.ProductCategory;
import springmvc.java.domain.ProductType;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface ProductDAO extends CrudRepository<Product, Long> {
    List<Product> findAll();

    @Query("Select p From Product p order by p.date desc ")
    List<Product> findAllOrderByDateDesc();

    List<Product> findByFkCategory(ProductCategory var1);

    List<Product> findByFkCategoryAndFkType(ProductCategory var1, ProductType var2);

    Product findByProductId(int id);
}
