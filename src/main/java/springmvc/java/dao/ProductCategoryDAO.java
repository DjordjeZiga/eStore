package springmvc.java.dao;

import org.springframework.data.repository.CrudRepository;
import springmvc.java.domain.ProductCategory;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface ProductCategoryDAO extends CrudRepository<ProductCategory, Long> {

    List<ProductCategory> findAll();
    ProductCategory findByCategoryName(String categoryName);
}
