package springmvc.java.dao;

import org.springframework.data.repository.CrudRepository;
import springmvc.java.domain.User;
import springmvc.java.domain.UserRole;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface UserRoleDAO extends CrudRepository<UserRole,Long> {

    List<UserRole> findAll();

    List<UserRole> findByFkUser(User user);
}

