package springmvc.java.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import springmvc.java.domain.CartOrder;
import springmvc.java.domain.OrderedProduct;
import springmvc.java.domain.Product;
import springmvc.java.domain.User;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by djordje_ziga on 4.3.17..
 */
public interface OrderedProductDAO extends CrudRepository<OrderedProduct,Long> {

    List<OrderedProduct> findByFkCartOrderByIdOrder(CartOrder cart);
    OrderedProduct findByIdOrder(Long id);

    @Query("update OrderedProduct set Quantity=:newQuantity where idOrder=:id")
    @Modifying
    @Transactional
    void updateQuantity(@Param("id") Long id,@Param("newQuantity") int newQuantity);
}
