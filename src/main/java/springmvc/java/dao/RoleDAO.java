package springmvc.java.dao;

import org.springframework.data.repository.CrudRepository;
import springmvc.java.domain.Role;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface RoleDAO extends CrudRepository<Role,Long> {

    Role findByRoleName(String name);
    List<Role> findAll();
}
