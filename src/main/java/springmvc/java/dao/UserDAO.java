package springmvc.java.dao;

import org.springframework.data.repository.CrudRepository;
import springmvc.java.domain.User;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface UserDAO extends CrudRepository<User,Long> {

    List<User> findAll();
    User findByUsername(String username);
}
