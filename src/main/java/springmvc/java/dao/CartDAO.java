package springmvc.java.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import springmvc.java.domain.CartOrder;
import springmvc.java.domain.User;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface CartDAO extends CrudRepository<CartOrder,Long> {

    @Query("select c from CartOrder c where c.cart=true and c.fkUser=:user")
    CartOrder findByFkUserWhereCartTrue(@Param("user") User user);
}
