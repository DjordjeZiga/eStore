package springmvc.java.controller;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springmvc.java.domain.Product;
import springmvc.java.service.ProductCategoryService;
import springmvc.java.service.ProductService;
import springmvc.java.service.ProductTypeService;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    ProductCategoryService productCategoryService;
    @Autowired
    ProductTypeService productTypeService;

    private static final String PATH = "/home/ziga/Documents/eStoreImg/";

    public ProductController() {
    }

    @RequestMapping({"/addProduct"})
    public ModelAndView addProduct(Product product, Model productModel) {
        List productCategories = this.productCategoryService.listAllProductCategory();
        List productTypes = this.productTypeService.listAllProductType();
        List products = this.productService.listAllProducts();
        productModel.addAttribute("productCategories", productCategories);
        productModel.addAttribute("productTypes", productTypes);
        productModel.addAttribute("products", products);
        return new ModelAndView("admin/addProduct", "productModel", productModel);
    }

    @RequestMapping(
            value = {"/saveProduct"},
            method = {RequestMethod.POST}
    )
    public ModelAndView saveProduct(@RequestParam("productName") String productName, @RequestParam("productModel") String productModel, @RequestParam("quantity") int quantity, @RequestParam("description") String description, @RequestParam("fkCategory") String categoryName, @RequestParam("price") int price, @RequestParam("fkType") String typeName, Model model, Locale locale, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
        Product product = new Product();
        product.setProductName(productName);
        product.setProductModel(productModel);
        product.setQuantity(quantity);
        product.setDescription(description);
        product.setPrice(price);
        product.setDate(new Date());
        product.setFkCategory(this.productCategoryService.findCategoryByName(categoryName));
        product.setFkType(this.productTypeService.findTypeByName(typeName));
        Product saved = this.productService.saveProduct(product);
        file.transferTo(new File("/home/djordje_ziga/Pictures/eStoreImg/" + saved.getProductId() + "." + FilenameUtils.getExtension(file.getOriginalFilename())));
        product = new Product();
        List productCategories = this.productCategoryService.listAllProductCategory();
        List productTypes = this.productTypeService.listAllProductType();
        List products = this.productService.listAllProducts();
        model.addAttribute("productCategories", productCategories);
        model.addAttribute("productTypes", productTypes);
        model.addAttribute("products", products);
        model.addAttribute("product", product);
        return new ModelAndView("admin/addProduct", "model", model);
    }
}
