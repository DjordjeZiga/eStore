package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.Product;
import springmvc.java.domain.ProductCategory;
import springmvc.java.domain.User;
import springmvc.java.service.ProductCategoryService;
import springmvc.java.service.ProductService;
import springmvc.java.service.ProductTypeService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class ListProductByCategoryController {
    @Autowired
    ProductService productService;
    @Autowired
    ProductCategoryService productCategoryService;
    @Autowired
    ProductTypeService productTypeService;

    public ListProductByCategoryController() {
    }

    @RequestMapping(
            value = {"/listProductsByCategory"},
            method = {RequestMethod.GET}
    )
    public ModelAndView listProductByCategory(Model model, @RequestParam("categoryName") String categoryName,@RequestParam(value = "currentPage") int currentPage) {
        ProductCategory productCategory = this.productCategoryService.findCategoryByName(categoryName);
        List products = this.productService.productsByCategory(productCategory);
        User user = new User();
        ArrayList productTypes = new ArrayList();

        for(int i = 0; i < products.size(); i++) {
            if(!productTypes.contains(((Product)products.get(i)).getFkType())) {
                productTypes.add(((Product)products.get(i)).getFkType());
            }
        }

        int maxPage;
        if(products.size() % 5 == 0) {
            maxPage = products.size() / 5;
        } else {
            maxPage = products.size() / 5 + 1;
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        model.addAttribute("username", username);
        model.addAttribute("products", products);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("maxPage", maxPage);
        model.addAttribute("productCategory", productCategory);
        model.addAttribute("productTypes", productTypes);
        model.addAttribute("user",user);
        model.addAttribute("categoryName",categoryName);
        return new ModelAndView("listProductsByCategory", "model", model);
    }

    @RequestMapping(
            value = {"/listPageCategory"},
            method = {RequestMethod.GET}
    )
    public ModelAndView listPageCategory(Model model, @RequestParam("categoryName") String categoryName, @RequestParam("pgNumber") int pgNumber) {
        ProductCategory productCategory = this.productCategoryService.findCategoryByName(categoryName);
        List products = this.productService.productsByCategory(productCategory);
        ArrayList productTypes = new ArrayList();

        int maxPage;
        for(maxPage = 0; maxPage < products.size(); ++maxPage) {
            if(!productTypes.contains(((Product)products.get(maxPage)).getFkType())) {
                productTypes.add(((Product)products.get(maxPage)).getFkType());
            }
        }

        if(products.size() % 10 == 0) {
            maxPage = products.size() / 10;
        } else {
            maxPage = products.size() / 10 + 1;
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        model.addAttribute("username", username);
        model.addAttribute("products", products);
        model.addAttribute("pageNumber", Integer.valueOf(pgNumber));
        model.addAttribute("maxPage", Integer.valueOf(maxPage));
        model.addAttribute("productCategory", productCategory);
        model.addAttribute("productTypes", productTypes);
        model.addAttribute("categoryName",categoryName);
        return new ModelAndView("listProductByCategory", "model", model);
    }
}
