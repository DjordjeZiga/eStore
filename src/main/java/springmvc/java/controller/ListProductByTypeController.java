package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.Product;
import springmvc.java.domain.ProductCategory;
import springmvc.java.domain.ProductType;
import springmvc.java.domain.User;
import springmvc.java.service.ProductCategoryService;
import springmvc.java.service.ProductService;
import springmvc.java.service.ProductTypeService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class ListProductByTypeController {
    @Autowired
    ProductService productService;
    @Autowired
    ProductCategoryService productCategoryService;
    @Autowired
    ProductTypeService productTypeService;

    public ListProductByTypeController() {
    }

    @RequestMapping(
            value = {"/listProductsByType"},
            method = {RequestMethod.GET}
    )
    public ModelAndView listPageType(Model model, @RequestParam("typeName") String typeName, @RequestParam("categoryName") String categoryName, @RequestParam("currentPage") int currentPage) {
        ProductType productType = this.productTypeService.findTypeByName(typeName);
        ProductCategory productCategory = this.productCategoryService.findCategoryByName(categoryName);
        List products = this.productService.productsByCategoryAndType(productCategory, productType);
        ArrayList productTypes = new ArrayList();
        ProductType type = this.productTypeService.findTypeByName(typeName);
        User user = new User();

        int maxPage;
        for(maxPage = 0; maxPage < products.size(); ++maxPage) {
            if(!productTypes.contains(((Product)products.get(maxPage)).getFkType())) {
                productTypes.add(((Product)products.get(maxPage)).getFkType());
            }
        }

        if(products.size() % 10 == 0) {
            maxPage = products.size() / 10;
        } else {
            maxPage = products.size() / 10 + 1;
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        model.addAttribute("username", username);
        model.addAttribute("typeName", typeName);
        model.addAttribute("categoryName",categoryName);
        model.addAttribute("products", products);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("maxPage", maxPage);
        model.addAttribute("productCategory", productCategory);
        model.addAttribute("productTypes", productTypes);
        model.addAttribute("type", type);
        model.addAttribute("user",user);
        return new ModelAndView("listProductsByType", "model", model);
    }
}
