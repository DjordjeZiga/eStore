package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.ProductType;
import springmvc.java.service.ProductTypeService;

import java.util.List;
import java.util.Locale;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class ProductTypeController {

    @Autowired
    ProductTypeService productTypeService;

    public ProductTypeController() {
    }

    @RequestMapping({"/addProductType"})
    public ModelAndView addProductType(ProductType productType, Model model) {
        List types = this.productTypeService.listAllProductType();
        model.addAttribute("types", types);
        return new ModelAndView("admin/addProductType");
    }

    @RequestMapping(
            value = {"/saveProductType"},
            method = {RequestMethod.POST}
    )
    public ModelAndView saveProductType(@ModelAttribute("productType") ProductType productType, Model model, Locale locale) {
        this.productTypeService.saveProductType(productType);
        productType = new ProductType();
        List types = this.productTypeService.listAllProductType();
        model.addAttribute("types", types);
        model.addAttribute("productType", productType);
        return new ModelAndView("admin/addProductType", "model", model);
    }
}
