package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.UserRole;
import springmvc.java.service.RoleService;
import springmvc.java.service.UserRoleService;
import springmvc.java.service.UserService;

import java.util.List;
import java.util.Locale;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class UserRoleController {
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    UserRoleService userRoleService;

    public UserRoleController() {
    }

    @RequestMapping({"/addRoleToUser"})
    ModelAndView addRoleToUser(UserRole userRole, Model userRoleModel) {
        List users = this.userService.listAllUser();
        List roles = this.roleService.listAllRole();
        List userRoles = this.userRoleService.listAllUserRoles();
        userRoleModel.addAttribute("userRoles", userRoles);
        userRoleModel.addAttribute("users", users);
        userRoleModel.addAttribute("roles", roles);
        return new ModelAndView("admin/addRoleToUser", "userRoleModel", userRoleModel);
    }

    @RequestMapping({"/saveUserRole"})
    ModelAndView saveUserRole(@ModelAttribute("userRole") UserRole userRole, Model model, Locale locale) {
        this.userRoleService.saveUserRole(userRole);
        List userRoles = this.userRoleService.listAllUserRoles();
        model.addAttribute("userRoles", userRoles);
        model.addAttribute("userRole", userRole);
        return new ModelAndView("admin/addRoleToUser", "model", model);
    }
}
