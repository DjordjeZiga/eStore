package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.Role;
import springmvc.java.service.RoleService;

import java.util.List;
import java.util.Locale;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class RoleController {
    @Autowired
    RoleService roleService;

    public RoleController() {
    }

    @RequestMapping({"/addRole"})
    public ModelAndView addRole(Role role, Model model) {
        List roles = this.roleService.listAllRole();
        model.addAttribute("roles", roles);
        return new ModelAndView("admin/addRole", "model", model);
    }

    @RequestMapping(
            value = {"/saveRole"},
            method = {RequestMethod.POST}
    )
    public ModelAndView saveRole(@ModelAttribute("role") Role role, Model model, Locale locale) {
        this.roleService.saveRole(role);
        role = new Role();
        List roles = this.roleService.listAllRole();
        model.addAttribute("roles", roles);
        model.addAttribute("role", role);
        return new ModelAndView("admin/addRole", "model", model);
    }
}