package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.ProductCategory;
import springmvc.java.service.ProductCategoryService;

import java.util.List;
import java.util.Locale;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class ProductCategoryController {

    @Autowired
    ProductCategoryService productCategoryService;

    @RequestMapping({"/addProductCategory"})
    public ModelAndView addProductCategory(ProductCategory productCategory, Model model) {
        List categories = this.productCategoryService.listAllProductCategory();
        model.addAttribute("categories", categories);
        return new ModelAndView("admin/addProductCategory", "model", model);
    }

    @RequestMapping({"/saveProductCategory"})
    public ModelAndView saveProductCategory(@ModelAttribute("productCategory") ProductCategory productCategory, Model model, Locale locale) {
        this.productCategoryService.saveProductCategory(productCategory);
        productCategory = new ProductCategory();
        List categories = this.productCategoryService.listAllProductCategory();
        model.addAttribute("categories", categories);
        model.addAttribute("productCategory", productCategory);
        return new ModelAndView("admin/addProductCategory", "model", model);
    }
}