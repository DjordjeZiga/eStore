package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.Role;
import springmvc.java.domain.User;
import springmvc.java.domain.UserRole;
import springmvc.java.service.RoleService;
import springmvc.java.service.UserRoleService;
import springmvc.java.service.UserService;

import java.util.Locale;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    UserRoleService userRoleService;

    @RequestMapping({"registration"})
    public ModelAndView registration() {
        User user = new User();
        return new ModelAndView("registration", "user", user);
    }

    @RequestMapping(
            value = {"/saveUser"},
            method = {RequestMethod.POST}
    )
    public ModelAndView saveUser(@ModelAttribute("user") User user, @RequestParam("confirmPassword") String confirmPassword, Model model, Locale locale) {
        if(user.getPassword().equals(confirmPassword)) {
            this.userService.saveUser(user);
            Role role = this.roleService.findRoleByName("user");
            UserRole userRole = new UserRole();
            userRole.setFkRole(role);
            userRole.setFkUser(user);
            this.userRoleService.saveUserRole(userRole);
            user = new User();
            model.addAttribute("user", user);
            return new ModelAndView("index", "model", model);
        } else {
            model.addAttribute("user", user);
            return new ModelAndView("index", "model", model);
        }
    }
}