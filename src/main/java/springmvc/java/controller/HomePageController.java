package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.User;
import springmvc.java.service.ProductCategoryService;
import springmvc.java.service.ProductService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class HomePageController{

    @Autowired
    ProductCategoryService productCategoryService;

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/")
    public ModelAndView homePage(Model model) {
        List products = productService.sixNewest();
        List categories = productCategoryService.listAllProductCategory();
        User user = new User();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        model.addAttribute("username", username);
        model.addAttribute("categories", categories);
        model.addAttribute("products", products);
        model.addAttribute("user",user);
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/imageDisplay{id}", method = RequestMethod.GET)
    public void doGet(HttpServletResponse response, @RequestParam(value = "id") int id) throws IOException {
        response.setContentType("image/jpeg");
        ServletOutputStream out = response.getOutputStream();
        FileInputStream fin = new FileInputStream("/home/djordje_ziga/Pictures/eStoreImg/" + id + ".jpg");
        BufferedInputStream bin = new BufferedInputStream(fin);
        BufferedOutputStream bout = new BufferedOutputStream(out);
        boolean ch = false;

        int ch1;
        while((ch1 = bin.read()) != -1) {
            bout.write(ch1);
        }

        bin.close();
        fin.close();
        bout.close();
        out.close();
    }
}
