package springmvc.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springmvc.java.domain.CartOrder;
import springmvc.java.domain.OrderedProduct;
import springmvc.java.domain.Product;
import springmvc.java.domain.User;
import springmvc.java.service.CartService;
import springmvc.java.service.OrderedProductService;
import springmvc.java.service.ProductService;
import springmvc.java.service.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by djordje_ziga on 10.2.17..
 */
@Controller
public class CartController {
    @Autowired
    UserService userService;
    @Autowired
    CartService cartService;
    @Autowired
    ProductService productService;
    @Autowired
    OrderedProductService orderedProductService;

    @RequestMapping({"/cart"})
    public ModelAndView cart(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        }else {
            username = principal.toString();
        }

        User user = this.userService.findUserByUsername(username);
        CartOrder cartOrder=cartService.findCartByUserIdWhereCartTrue(user);

        List<OrderedProduct> orderedProducts=new ArrayList<>();

        if (cartOrder !=null){
            orderedProducts=orderedProductService.listOrderedProductByFkCart(cartOrder);
        }
        model.addAttribute("username",username);
        model.addAttribute("orderedProducts",orderedProducts);
        return new ModelAndView("cart","model",model);
    }

    public void addCartToUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = null;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        }
        User user = this.userService.findUserByUsername(username);
        CartOrder cartOrder = new CartOrder();
        cartOrder.setFkUser(user);
        cartOrder.setDate(new Date());
        cartOrder.setCart(true);
        cartOrder.setRealised(false);
        if(this.cartService.findCartByUserIdWhereCartTrue(user) == null) {
            this.cartService.saveCart(cartOrder);
        }else{
            cartService.findCartByUserIdWhereCartTrue(user).setDate(new Date());
        }

    }

    @RequestMapping(value = {"/addProductToCart"})
    public void addProductToCart(@RequestParam(value = "id") int id) {
        addCartToUser();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = ((UserDetails)principal).getUsername();

            User user=userService.findUserByUsername(username);

            CartOrder cart=cartService.findCartByUserIdWhereCartTrue(user);

            OrderedProduct orderedProduct=new OrderedProduct();

            orderedProduct.setFkCart(cart);
            orderedProduct.setFkProduct(productService.productById(id));
            orderedProduct.setQuantity(1);
            orderedProductService.save(orderedProduct);
        }
    }

    @RequestMapping(value = {"/updateQuantity"}, method = RequestMethod.POST)
    public ModelAndView QuantityUpdate(@RequestParam(value = "id") Long id,@RequestParam(value = "newQuantity") int newQuantity,Model model){
        update(id,newQuantity);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = null;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        }

        User user = this.userService.findUserByUsername(username);
        CartOrder cartOrder=cartService.findCartByUserIdWhereCartTrue(user);

        List<OrderedProduct> orderedProducts=orderedProductService.listOrderedProductByFkCart(cartOrder);
        model.addAttribute("orderedProducts",orderedProducts);
        return new ModelAndView("cart","model",model);
    }

    public void update(Long id,int newQuantity){
        OrderedProduct orderedProduct=orderedProductService.findById(id);
        Product product=productService.productById(orderedProduct.getFkProduct().getProductId());
        if(newQuantity>orderedProduct.getQuantity() && orderedProduct.getQuantity()<product.getQuantity()) {
            orderedProductService.updateQuantity(id, newQuantity);
        }
        if(newQuantity<orderedProduct.getQuantity() && orderedProduct.getQuantity()>1) {
            orderedProductService.updateQuantity(id, newQuantity);
        }

    }
}
