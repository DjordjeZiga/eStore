package springmvc.java.service;

import springmvc.java.domain.CartOrder;
import springmvc.java.domain.User;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface CartService {

    CartOrder saveCart(CartOrder cartOrder);
    CartOrder findCartByUserIdWhereCartTrue(User user);
}
