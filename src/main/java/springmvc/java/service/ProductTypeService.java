package springmvc.java.service;

import springmvc.java.domain.ProductType;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface ProductTypeService {

    List<ProductType> listAllProductType();

    ProductType saveProductType(ProductType productType);
    ProductType findTypeByName(String typeName);
}
