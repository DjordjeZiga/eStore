package springmvc.java.service;

import springmvc.java.domain.CartOrder;
import springmvc.java.domain.OrderedProduct;

import java.util.List;

/**
 * Created by djordje_ziga on 4.3.17..
 */
public interface OrderedProductService {

    OrderedProduct save(OrderedProduct orderedProduct);
    List<OrderedProduct> listOrderedProductByFkCart(CartOrder cart);
    OrderedProduct findById(Long id);
    void updateQuantity(Long id,int newQuantity);
}
