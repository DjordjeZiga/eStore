package springmvc.java.service;

import springmvc.java.domain.Product;
import springmvc.java.domain.ProductCategory;
import springmvc.java.domain.ProductType;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface ProductService {

    Product saveProduct(Product product);
    List<Product> listAllProducts();
    List<Product> sixNewest();
    List<Product> productsByCategory(ProductCategory category);
    List<Product> productsByCategoryAndType(ProductCategory productCategory,ProductType productType);
    Product productById(int id);
}
