package springmvc.java.service;

import springmvc.java.domain.User;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface UserService {

    User saveUser(User user);
    List<User> listAllUser();
    User findUserByUsername(String username);

}
