package springmvc.java.service;

import springmvc.java.domain.Role;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface RoleService {

    Role saveRole(Role role);
    Role findRoleByName(String name);
    List<Role> listAllRole();
}
