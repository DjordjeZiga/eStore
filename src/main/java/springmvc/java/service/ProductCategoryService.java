package springmvc.java.service;

import springmvc.java.domain.ProductCategory;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface ProductCategoryService {

    List<ProductCategory> listAllProductCategory();

    ProductCategory saveProductCategory(ProductCategory productCategory);
    ProductCategory findCategoryByName(String categoryName);
}
