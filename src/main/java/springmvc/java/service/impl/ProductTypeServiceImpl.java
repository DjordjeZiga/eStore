package springmvc.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springmvc.java.dao.ProductTypeDAO;
import springmvc.java.domain.ProductType;
import springmvc.java.service.ProductTypeService;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Service
public class ProductTypeServiceImpl implements ProductTypeService {

    @Autowired
    ProductTypeDAO productTypeDAO;

    @Override
    public List<ProductType> listAllProductType() {
        return productTypeDAO.findAll();
    }

    @Override
    public ProductType saveProductType(ProductType productType) {
        return productTypeDAO.save(productType);
    }

    @Override
    public ProductType findTypeByName(String typeName) {
        return productTypeDAO.findByTypeName(typeName);
    }
}
