package springmvc.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springmvc.java.dao.OrderedProductDAO;
import springmvc.java.domain.CartOrder;
import springmvc.java.domain.OrderedProduct;
import springmvc.java.service.OrderedProductService;

import java.util.List;

/**
 * Created by djordje_ziga on 4.3.17..
 */
@Service
public class OrderedProductServiceImpl implements OrderedProductService {

    @Autowired
    OrderedProductDAO orderedProductDAO;


    @Override
    public OrderedProduct save(OrderedProduct orderedProduct) {
        return orderedProductDAO.save(orderedProduct);
    }

    @Override
    public List<OrderedProduct> listOrderedProductByFkCart(CartOrder cart) {
        return orderedProductDAO.findByFkCartOrderByIdOrder(cart);
    }

    @Override
    public OrderedProduct findById(Long id) {
        return orderedProductDAO.findByIdOrder(id);
    }

    @Override
    public void updateQuantity(Long id, int newQuantity) {
        orderedProductDAO.updateQuantity(id,newQuantity);
    }
}
