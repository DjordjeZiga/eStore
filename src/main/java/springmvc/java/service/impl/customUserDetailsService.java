package springmvc.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import springmvc.java.dao.UserDAO;
import springmvc.java.domain.Role;
import springmvc.java.domain.User;
import springmvc.java.domain.UserRole;
import springmvc.java.service.RoleService;
import springmvc.java.service.UserRoleService;
import springmvc.java.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by djordje_ziga on 3.3.17..
 */
@Service("customUserDetailsService")
public class customUserDetailsService implements UserDetailsService{

    @Autowired
    UserService userService;

    @Autowired
    UserRoleService userRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user=userService.findUserByUsername(username);
        if(user==null){
            throw new UsernameNotFoundException("Username not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),true,true,true,true,getGrantedAuthority(user));

    }

    private List<GrantedAuthority> getGrantedAuthority(User user){
        List<GrantedAuthority> authorities= new ArrayList<GrantedAuthority>();
        List<UserRole> userRoles=userRoleService.listAllUserRolesByUser(user);
        for(UserRole userRole : userRoles){
            authorities.add(new SimpleGrantedAuthority("ROLE_"+userRole.getFkRole().getRoleName().toUpperCase()));
        }
        return authorities;
    }
}
