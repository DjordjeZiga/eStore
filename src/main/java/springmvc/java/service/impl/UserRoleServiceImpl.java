package springmvc.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springmvc.java.dao.UserRoleDAO;
import springmvc.java.domain.User;
import springmvc.java.domain.UserRole;
import springmvc.java.service.UserRoleService;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    UserRoleDAO userRoleDAO;

    @Override
    public UserRole saveUserRole(UserRole userRole) {
        return userRoleDAO.save(userRole);
    }

    @Override
    public List<UserRole> listAllUserRoles() {
        return userRoleDAO.findAll();
    }

    @Override
    public List<UserRole> listAllUserRolesByUser(User user) {
        return userRoleDAO.findByFkUser(user);
    }
}
