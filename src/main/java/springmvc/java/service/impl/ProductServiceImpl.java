package springmvc.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springmvc.java.dao.ProductDAO;
import springmvc.java.domain.Product;
import springmvc.java.domain.ProductCategory;
import springmvc.java.domain.ProductType;
import springmvc.java.service.ProductService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDAO productDAO;

    @Override
    public Product saveProduct(Product product) {
        return productDAO.save(product);
    }

    @Override
    public List<Product> listAllProducts() {
        return productDAO.findAll();
    }

    @Override
    public List<Product> sixNewest() {
        List<Product> products=productDAO.findAllOrderByDateDesc();
        if (!products.isEmpty()) {
            List<Product> sixNewest = new ArrayList<>();
            if (products.size()>5) {
                for (int i = 0; i < 6; i++) {
                    sixNewest.add(products.get(i));
                }
            }else
                for (int i = 0; i < products.size(); i++) {
                    sixNewest.add(products.get(i));
                }
            return sixNewest;
        }
        else return null;
    }

    @Override
    public List<Product> productsByCategory(ProductCategory category) {
        return productDAO.findByFkCategory(category);
    }

    @Override
    public List<Product> productsByCategoryAndType(ProductCategory productCategory, ProductType productType) {
        return productDAO.findByFkCategoryAndFkType(productCategory,productType);
    }

    @Override
    public Product productById(int id) {
        return productDAO.findByProductId(id);
    }
}
