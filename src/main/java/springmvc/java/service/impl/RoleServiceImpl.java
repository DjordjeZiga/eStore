package springmvc.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springmvc.java.dao.RoleDAO;
import springmvc.java.domain.Role;
import springmvc.java.service.RoleService;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleDAO roleDAO;

    @Override
    public Role saveRole(Role role) {
        return roleDAO.save(role);
    }

    @Override
    public Role findRoleByName(String name) {
        return roleDAO.findByRoleName(name);
    }

    @Override
    public List<Role> listAllRole() {
        return roleDAO.findAll();
    }
}
