package springmvc.java.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springmvc.java.dao.CartDAO;
import springmvc.java.domain.CartOrder;
import springmvc.java.domain.User;
import springmvc.java.service.CartService;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    CartDAO cartDAO;

    @Override
    public CartOrder saveCart(CartOrder cartOrder) {
        return cartDAO.save(cartOrder);
    }

    @Override
    public CartOrder findCartByUserIdWhereCartTrue(User user) {
        return cartDAO.findByFkUserWhereCartTrue(user);
    }
}
