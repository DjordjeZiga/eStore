package springmvc.java.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springmvc.java.dao.ProductCategoryDAO;
import springmvc.java.domain.ProductCategory;
import springmvc.java.service.ProductCategoryService;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Autowired
    ProductCategoryDAO productCategoryDAO;

    @Override
    public List<ProductCategory> listAllProductCategory() {
        return productCategoryDAO.findAll();
    }

    @Override
    public ProductCategory saveProductCategory(ProductCategory productCategory) {
        return productCategoryDAO.save(productCategory);
    }

    @Override
    public ProductCategory findCategoryByName(String categoryName) {
        return productCategoryDAO.findByCategoryName(categoryName);
    }
}
