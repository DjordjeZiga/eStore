package springmvc.java.service;

import springmvc.java.domain.User;
import springmvc.java.domain.UserRole;

import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
public interface UserRoleService{
    UserRole saveUserRole(UserRole userRole);
    List<UserRole> listAllUserRoles();
    List<UserRole> listAllUserRolesByUser(User user);
}
