package springmvc.java.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Entity
public class ProductType {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            nullable = false
    )
    private int typeId;
    @Column(
            nullable = false
    )
    private String typeName;
    @OneToMany(
            mappedBy = "fkType",
            fetch = FetchType.EAGER
    )
    private List<Product> product;

    public ProductType() {
    }

    public int getTypeId() {
        return this.typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}

