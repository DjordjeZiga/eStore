package springmvc.java.domain;

import javax.persistence.*;

/**
 * Created by djordje_ziga on 9.2.17..
 */

@Entity
public class UserRole {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            nullable = false
    )
    private Long roleId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkRole", nullable = false)
    private Role fkRole;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkUser", nullable = false)
    private User fkUser;

    public Role getFkRole() {
        return this.fkRole;
    }

    public void setFkRole(Role fkRole) {
        this.fkRole = fkRole;
    }

    public User getFkUser() {
        return this.fkUser;
    }

    public void setFkUser(User fkUser) {
        this.fkUser = fkUser;
    }

    public Long getRoleId() {
        return this.roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }


}
