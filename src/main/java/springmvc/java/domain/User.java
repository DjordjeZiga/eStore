package springmvc.java.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Entity
@Table(name = "eStore_user")
public class User {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            nullable = false
    )
    private Long userId;
    @Column(
            nullable = false
    )
    private String name;
    @Column(
            nullable = false
    )
    private String lastname;
    @Column(
            nullable = false
    )
    private String zip;
    @Column(
            nullable = false
    )
    private String username;
    @Column(
            nullable = false
    )
    private String email;
    @Column(
            nullable = false
    )
    private String city;
    @Column(
            nullable = false
    )
    private String street;
    @Column(
            nullable = false
    )
    private String number;
    @Column(
            nullable = false
    )
    private String password;
    @Column(
            nullable = false
    )
    private String phoneNumber;
    @OneToMany(
            mappedBy = "fkUser",
            fetch = FetchType.EAGER
    )
    private Set<UserRole> users = new HashSet<>();

    @OneToMany(
            mappedBy = "fkUser",
            fetch = FetchType.EAGER
    )
    private List<CartOrder> cartOrder;

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCity() {
        return this.city;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
