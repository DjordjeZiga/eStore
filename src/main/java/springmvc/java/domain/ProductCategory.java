package springmvc.java.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Entity
public class ProductCategory {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            nullable = false
    )
    private int categoryId;
    @Column(
            nullable = false
    )
    private String categoryName;
    @OneToMany(
            mappedBy = "fkCategory",
            fetch = FetchType.EAGER
    )
    private List<Product> product;

    public ProductCategory() {
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(int productId) {
        this.categoryId = productId;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String productName) {
        this.categoryName = productName;
    }
}
