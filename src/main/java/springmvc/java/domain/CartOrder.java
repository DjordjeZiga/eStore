package springmvc.java.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Entity
public class CartOrder {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            nullable = false
    )
    private Long cartId;
    @ManyToOne(
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "fkUser",
            nullable = false
    )
    private User fkUser;
    @Column(
            nullable = false
    )
    private boolean cart = true;
    @Column(
            nullable = false
    )
    private boolean realised = false;
    @Column
    private Date date;
    @OneToMany(
            mappedBy = "fkCart",
            fetch = FetchType.EAGER
    )
    private List<OrderedProduct> orderedProduct;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkAddress", nullable = false)
    private Address fkAddress;

    public Long getCartId() {
        return this.cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public User getFkUser() {
        return this.fkUser;
    }

    public void setFkUser(User fkUser) {
        this.fkUser = fkUser;
    }

    public boolean isCart() {
        return this.cart;
    }

    public void setCart(boolean cart) {
        this.cart = cart;
    }

    public boolean isRealised() {
        return this.realised;
    }

    public void setRealised(boolean realised) {
        this.realised = realised;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Address getFkAddress() {
        return fkAddress;
    }

    public void setFkAddress(Address fkAddress) {
        this.fkAddress = fkAddress;
    }
}
