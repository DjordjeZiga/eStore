package springmvc.java.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Entity
public class Role {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            unique = true,
            nullable = false
    )
    private Long roleId;
    @Column(
            nullable = false
    )
    private String roleName;
    @OneToMany(
            mappedBy = "fkRole",
            fetch = FetchType.EAGER
    )
    private List<UserRole> userRoles;

    public Role() {
    }

    public Long getRoleId() {
        return this.roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}