package springmvc.java.domain;

import javax.persistence.*;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Entity
public class OrderedProduct {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            nullable = false
    )
    private Long idOrder;
    @ManyToOne(
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "fkCart",
            nullable = false
    )
    private CartOrder fkCart;
    @ManyToOne(
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "fkProduct",
            nullable = false
    )
    private Product fkProduct;

    @Column(nullable = false)
    private int quantity;

    public CartOrder getFkCart() {
        return this.fkCart;
    }

    public void setFkCart(CartOrder fkCart) {
        this.fkCart = fkCart;
    }

    public Product getFkProduct() {
        return this.fkProduct;
    }

    public void setFkProduct(Product fkProduct) {
        this.fkProduct = fkProduct;
    }

    public Long getIdOrder() {
        return this.idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
