package springmvc.java.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by djordje_ziga on 9.2.17..
 */
@Entity
public class Product {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            nullable = false
    )
    private int productId;
    @Column(
            nullable = false
    )
    private String productName;
    @ManyToOne(
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "fkCategory",
            nullable = false
    )
    private ProductCategory fkCategory;
    @ManyToOne(
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "fkType",
            nullable = false
    )
    private ProductType fkType;
    @Column(
            nullable = false
    )
    private String productModel;
    @Column(
            nullable = false
    )
    private int price;
    @Column(
            nullable = false
    )
    private Date date;
    @Column(
            nullable = false
    )
    private int quantity;
    @Column(
            nullable = false
    )
    private String description;
    @OneToMany(
            mappedBy = "fkProduct",
            fetch = FetchType.EAGER
    )
    private List<OrderedProduct> orderedProduct;

    public Product() {
    }

    public int getProductId() {
        return this.productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductCategory getFkCategory() {
        return this.fkCategory;
    }

    public void setFkCategory(ProductCategory fkCategory) {
        this.fkCategory = fkCategory;
    }

    public ProductType getFkType() {
        return this.fkType;
    }

    public void setFkType(ProductType fkType) {
        this.fkType = fkType;
    }

    public String getProductModel() {
        return this.productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
