<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: djordje_ziga
  Date: 10.2.17.
  Time: 18.13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Products</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">E-store</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
            </ul>
            <ul class="nav navbar-nav navbar-right" style="padding-right: 10px">

                <li><a href="#"><i class="glyphicon glyphicon-user"></i> ${username}</a></li>
                <li><a href="${pageContext.request.contextPath}/cart"><i class="glyphicon glyphicon-shopping-cart"></i> Cart</a></li>
                <sec:authorize access="hasRole('ADMIN')">
                    <li><a href="${pageContext.request.contextPath}/admin">Admin page</a></li>
                </sec:authorize>
                <sec:authorize access="isAuthenticated()">
                    <li><a href=/logout><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-log-in"></i> Log in</a>
                        <ul class="dropdown-menu" role="menu">
                            <div class="col-lg-12">
                                <form method="post" class="form-horizontal" action="/login">
                                    <div class="form-group-sm">
                                        <span for="email">Email</span>
                                        <li><input type="text" class="form-control" id="username" name="username"/></li>
                                    </div>
                                    <div class="form-group-sm">
                                        <span for="password">Password</span>
                                        <li><input type="password" class="form-control " id="password"
                                                   name="password"/></li>
                                    </div>
                                    <div class="form-group-sm">
                                        <br>
                                        <button type="submit" class=" col-lg-12 btn btn-group-sm btn-primary">Login</button>
                                    </div>
                                </form>
                            </div>
                        </ul>
                    </li>
                </sec:authorize>
            </ul>
        </div>
    </div>
</nav>
    <table style="width: 100%" class="table table-bordered">
        <tr>
            <th style="width: 20%">Picture</th>
            <th style="width: 60%">Product name</th>
            <th style="width: 20%" colspan="2">Quantity</th>
            <th style="width: 10%">Price</th>
        </tr>
        <c:forEach var="product" items="${orderedProducts}">
            <tr>
                <td style="width: 20%" rowspan="2" align="center">
                <img style="height: 50px" src="imageDisplay?id=${product.getFkProduct().getProductId()}" alt="slika">
                </td>
                <td style="width: 60%;text-align: left;vertical-align: middle" rowspan="2">${product.getFkProduct().getProductName()}</td>
                <td style="width: 5%;text-align: left;vertical-align: middle" rowspan="2">${product.getQuantity()}</td>
                <td style="width: 5%;padding: 0">
                    <form action="/updateQuantity" method="post">
                        <input value="${product.getIdOrder()}" name="id" id="id" hidden/>
                        <input value="${product.getQuantity()+1}" name="newQuantity" id="newQuantity" hidden/>
                        <button style="width: 100%;" class="btn btn-success">+</button>
                    </form>
                </td>
                <td style="width: 10%;text-align: left;vertical-align: middle" rowspan="2">${product.getFkProduct().getPrice()}</td>
            </tr>
            <tr class="danger">
                <td  style="width: 5%;padding:0">
                    <form action="/updateQuantity" method="post">
                        <input value="${product.getIdOrder()}" name="id" id="id1" hidden/>
                        <input value="${product.getQuantity()-1}" name="newQuantity" id="newQuantity1" hidden/>
                        <button style="width: 100%;" class="btn btn-primary">-</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="2" style="border-left: hidden;border-bottom: hidden" ></td>
            <td colspan="2">Total</td>
            <td>Price</td>
        </tr>
    </table>
</div>
</body>
</html>
