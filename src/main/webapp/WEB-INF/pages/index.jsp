<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: djordje_ziga
  Date: 10.2.17.
  Time: 18.13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">E-store</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
            </ul>
            <ul class="nav navbar-nav navbar-right" style="padding-right: 10px">

                <li><a href="#"><i class="glyphicon glyphicon-user"></i> ${username}</a></li>
                <li><a href="${pageContext.request.contextPath}/cart"><i class="glyphicon glyphicon-shopping-cart"></i> Cart</a></li>
                <sec:authorize access="hasRole('ADMIN')">
                    <li><a href="${pageContext.request.contextPath}/admin">Admin page</a></li>
                </sec:authorize>
                <sec:authorize access="isAuthenticated()">
                    <li><a href=/logout><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-log-in"></i> Log in</a>
                    <ul class="dropdown-menu" role="menu">
                        <div class="col-lg-12">
                            <form method="post" class="form-horizontal" action="/login">
                                <div class="form-group-sm">
                                    <span for="email">Email</span>
                                    <li><input type="text" class="form-control" id="username" name="username"/></li>
                                </div>
                                <div class="form-group-sm">
                                    <span for="password">Password</span>
                                    <li><input type="password" class="form-control " id="password"
                                               name="password"/></li>
                                </div>
                                <div class="form-group-sm">
                                    <br>
                                    <button type="submit" class=" col-lg-12 btn btn-group-sm btn-primary">Login</button>
                                </div>
                            </form>
                        </div>
                    </ul>
                </li>
                <li><a href="#" data-toggle="modal" data-target="#Signup" ><i class="glyphicon glyphicon-pencil"></i> Sign up</a></li>
                </sec:authorize>
            </ul>
        </div>
    </div>
</nav>
    <div class="row">
        <div class="col-lg-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a>Categories</a></li>
                <c:forEach var="category" items="${categories}">
                    <li value="${category.getCategoryName()}"><a href="${pageContext.request.contextPath}/listProductsByCategory?categoryName=${category.getCategoryName()}&currentPage=1">${category.getCategoryName()}</a></li>
                </c:forEach>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="row carousel-holder">
                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="http://placehold.it/850x300" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="http://placehold.it/850x300" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="http://placehold.it/850x300" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <c:forEach var="product" items="${products}">
                    <div class="col-lg-4">
                        <div class="thumbnail">
                            <img style="height: 150px" src="imageDisplay?id=${product.getProductId()}" alt="slika">
                            <div class="caption">
                                <h4 class="pull-right">${product.getPrice()}</h4>
                                <h4><a href="#">${product.getProductName()}</a></h4>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade container" id="Signup" role="dialog">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="modal-content col-lg-10 col-lg-offset-1">
                <form:form class="form-horizontal" modelAttribute="user" action="/saveUser">
                    <h2>Registration</h2>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="name" class="control-label">Name</label>
                            <form:input type="text" class="form-control" id="name" path="name"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="lastname" class="control-label">Lastname</label>
                            <form:input type="text" class="form-control" id="lastname" path="lastname"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-4">
                            <label for="zip" class="control-label">Zip</label>
                            <form:input type="text" class="form-control" id="zip" path="zip"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="username" class="control-label">Username</label>
                            <form:input type="text" class="form-control" id="username" path="username"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="email" class="control-label">Email</label>
                            <form:input type="text" class="form-control" id="email" path="email"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="city" class="control-label">City</label>
                            <form:input type="text" class="form-control" id="city" path="city"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-10">
                            <label for="street" class="control-label">Street</label>
                            <form:input type="text" class="form-control" id="street" path="street"></form:input>
                        </div>
                        <div class="col-lg-2">
                            <label for="number" class="control-label">Number</label>
                            <form:input type="text" class="form-control" id="number" path="number"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="phoneNumber" class="control-label">Phone number</label>
                            <form:input type="text" class="form-control" id="phoneNumber" path="phoneNumber"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="password" class="control-label">Password</label>
                            <form:input type="password" class="form-control" id="password" path="password"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="confirmPassword" class="control-label">Confirm password</label>
                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-group-sm">Submit</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
