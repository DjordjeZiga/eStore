<%--
  Created by IntelliJ IDEA.
  User: djordje_ziga
  Date: 11.2.17.
  Time: 00.27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}/admin">Admin ${pageContext.request.userPrincipal.name.toString()}</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">

            </ul>
            <ul class="nav navbar-nav navbar-right" style="padding-right: 10px">
                <li><a href="${pageContext.request.contextPath}/">User page</a></li>
                <li><a href=/logout><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<a href="${pageContext.request.contextPath}/addProductCategory">addCategory</a><br>
<a href="${pageContext.request.contextPath}/addProductType">addType</a><br>
<a href="${pageContext.request.contextPath}/addProduct">addProduct</a><br>
<a href="${pageContext.request.contextPath}/addRole">addRole</a><br>
<a href="${pageContext.request.contextPath}/addRoleToUser">addRoleToUser</a>
</div>
</body>
</html>
