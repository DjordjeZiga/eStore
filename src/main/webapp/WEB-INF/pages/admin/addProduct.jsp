<%--
  Created by IntelliJ IDEA.
  User: djordje_ziga
  Date: 11.2.17.
  Time: 20.39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Add Product</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.request.contextPath}/admin">Admin ${pageContext.request.userPrincipal.name.toString()}</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">

                </ul>
                <ul class="nav navbar-nav navbar-right" style="padding-right: 10px">
                    <li><a href="${pageContext.request.contextPath}/">User page</a></li>
                    <li><a href=/logout><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="row">
        <div class="col-lg-8">
            <table class="table table-striped table-hover ">
                <tr class="danger">
                    <th>Id</th>
                    <th>Product name</th>
                    <th>Prduct category</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>
                <c:forEach var="product" items="${products}">
                    <tr>
                        <td>${product.getProductId()}</td>
                        <td>${product.getProductName()}</td>
                        <td>${product.getFkCategory().getCategoryName()}</td>
                        <td>${product.getQuantity()}</td>
                        <td>${product.getPrice()}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div class="col-lg-4">
            <form class="form-horizontal" action="/saveProduct" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>Add product</legend>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="productName" class="control-label">Name</label>
                            <input type="text" class="form-control" name="productName" id="productName"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="productModel" class="control-label">Model</label>
                            <input type="text" class="form-control" name="productModel" id="productModel"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="quantity" class="control-label">Quantity</label>
                            <input type="text" class="form-control" name="quantity" id="quantity"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="price" class="control-label">Price</label>
                            <input type="text" class="form-control" name="price" id="price"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="fkCategory" class="control-label">Category</label>
                            <select class="form-control" name="fkCategory" id="fkCategory">
                                <c:forEach var="category" items="${productCategories}">
                                    <option value="${category.getCategoryName()}">${category.getCategoryName()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="fkType" class="control-label">Type</label>
                            <select class="form-control" name="fkType" id="fkType">
                                <c:forEach var="type" items="${productTypes}">
                                    <option value="${type.getTypeName()}">${type.getTypeName()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="description" class="control-label">Description</label>
                            <textarea type="text" cols="15" class="form-control" name="description" id="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="file" class="control-label">Image</label>
                            <input type="file" class="form-control" name="file" id="file"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-group-sm">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
</html>
