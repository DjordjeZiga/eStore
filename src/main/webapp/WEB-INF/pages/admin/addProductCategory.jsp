<%--
  Created by IntelliJ IDEA.
  User: djordje_ziga
  Date: 11.2.17.
  Time: 20.39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Add category</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.request.contextPath}/admin">Admin ${pageContext.request.userPrincipal.name.toString()}</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">

                </ul>
                <ul class="nav navbar-nav navbar-right" style="padding-right: 10px">
                    <li><a href="${pageContext.request.contextPath}/">User page</a></li>
                    <li><a href=/logout><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="row">
        <div class="col-lg-9">
            <table class="table table-striped table-hover ">
                <tr class="danger">
                    <th>Id</th>
                    <th>Category name</th>
                </tr>
                <c:forEach var="category" items="${categories}">
                    <tr>
                        <td>${category.getCategoryId()}</td>
                        <td>${category.getCategoryName()}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div class="col-lg-3">
            <form:form class="form-horizontal" modelAttribute="productCategory" action="/saveProductCategory">
                <fieldset>
                    <legend>Add category</legend>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="categoryName" class="control-label">Category name</label>
                            <form:input type="text" class="form-control" id="categoryName" path="categoryName"></form:input>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-group-sm">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
